---
layout: markdown_page
title: "William Chia's README"
---

## William Chia README

Sr. Product Marketing Manager, Cloud Native & GitOps / Product Management Intern Monitor:Health

## Related pages

* [linkedin.com/in/williamchia/](https://www.linkedin.com/in/williamchia/)
* [twitter.com/thewilliamchia](https://twitter.com/thewilliamchia)
* [gitlab.com/williamchia](https://gitlab.com/williamchia)

## About me

* I am a big fan of the Iteration value and [Minimum Viable Change](/handbook/values/#minimum-viable-change-mvc)
* I also enjoy being meta
